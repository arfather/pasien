<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PasienController;

Route::get('/', [AuthController::class, 'showFormLogin'])->name('login');
Route::get('login', [AuthController::class, 'showFormLogin'])->name('login');
Route::post('login', [AuthController::class, 'login']);
Route::get('register', [AuthController::class, 'showFormRegister'])->name('register');
Route::post('register', [AuthController::class, 'register']);

Route::group(['middleware' => 'auth'], function () {
    Route::get('anyData', [HomeController::class, 'anyData'])->name('anyData');
    Route::get('home', [HomeController::class, 'index'])->name('home');
    Route::get('profile/{id}', [HomeController::class, 'profile'])->name('profile');
    Route::get('cetak/{id}', [HomeController::class, 'cetak'])->name('cetak');
    Route::get('cetak-sertifikat/{nama}/{tanggalLahir}', [HomeController::class, 'cetakSertifikat'])->name('cetakSertifikat');
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    Route::post('pendaftaran', [PasienController::class, 'pendaftaran']);
    Route::post('checkup', [PasienController::class, 'checkup']);
});
