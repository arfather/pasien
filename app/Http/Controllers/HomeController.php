<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;
class HomeController extends Controller
{
    public function index()
    {
      $jumlahPasien = DB::table('pasien')->count();
      $jumlahPasienDirawat = DB::table('pasien')->where('flag', true)->count();
      $jumlahPasienSembuh = DB::table('pasien')->where('flag', false)->count();
        return view('home', [
          'pasien' => datatables()->of(DB::table('pasien'))->toJson(),
          'jumlahPasien' => $jumlahPasien,
          'jumlahPasienDirawat' => $jumlahPasienDirawat,
          'jumlahPasienSembuh' => $jumlahPasienSembuh,
      ]);
    }
    public function cetak($id)
    {
      $dataPasien = DB::table('pasien')->where('id', $id);
      if (count($dataPasien->get())) {
        if ($dataPasien->get()[0]->flag) {
          $dataPasien->update(['flag' => false]);
        }
        $pdf = PDF::loadview('cetak', ['namaPasien' => $dataPasien->get()[0]->nama]);
        return $pdf->download('sertifikat-pasien.pdf');
        // return view('cetak');
      }else{
        $dataPasien = null;
        return "Data Pasien Tidak Ditemukan";
      }
    }
    public function cetakSertifikat($nama, $tanggalLahir){
      $dataPasien = DB::table('pasien')->where('nama', $nama)->where('tanggalLahir', $tanggalLahir);
      if (count($dataPasien->get())) {
        if ($dataPasien->get()[0]->flag) {
          return "Pasien Belum dinyatakan Sembuh";
        }else {
          $pdf = PDF::loadview('cetak', ['namaPasien' => $dataPasien->get()[0]->nama]);
          return $pdf->download('sertifikat-pasien.pdf');
        }
        // return view('cetak');
      }else{
        $dataPasien = null;
        return "Data Pasien Tidak Ditemukan";
      }
    }
    public function Profile($id){
      $pasien = DB::table('pasien')->where('id', $id)->get();
      $perawatan = DB::table('perawatan')->where('idPasien', $id)->get();
      if (count($pasien)) {
        return view('profile', [
          'pasien' => $pasien[0],
          'perawatan' => $perawatan
        ]);
      }else{
        $pasien = null;
        return "Data Pasien Tidak Ditemukan";
      }
    }
}
