<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class PasienController extends Controller
{
    function pendaftaran(Request $request){
      $payload = json_decode($request->getContent(), true);
      $parseData = [];
      foreach ($payload as $key => $value) {
        $parseData[$value['name']] = $value['value'];
      }

      $umur = date_diff(date_create($parseData["tanggalLahir"]), date_create('now'))->y;

      DB::table('pasien')->insert([
    		'nama' => $parseData["nama"],
    		'tanggalLahir' => $parseData["tanggalLahir"],
        'alamat' => $parseData["alamat"],
    		'gejala' => $parseData["gejala"],
        'lastCheckUp' => $parseData["tanggalCheckUp"],
        'type' => $parseData["type"],
        'vendor' => $parseData["vendor"],
        'status' => $parseData["status"],
        'namaVendor' => $parseData["namaVendor"],
        'created_at' => date('Y-m-d'),
        'flag' => true,
        'umur' => $umur
    	]);

      $arr = array (
        'status'=> "OK",
        'response'=> 200,
        'message'=> "pasien berhasil didaftarkan"
      );
      return json_encode($arr);
    }
    function checkup(Request $request){
      $payload = json_decode($request->getContent(), true);
      $parseData = [];
      foreach ($payload as $key => $value) {
        $parseData[$value['name']] = $value['value'];
      }
      DB::table('perawatan')->insert([
    		'idPasien' => $parseData["idPasien"],
    		'tanggalCheckUp' => date('Y-m-d'),
        'minumObat' => $parseData["minumObat"],
    		'keteranganMinumObat' => $parseData["keteranganMinumObat"],
        'olahraga' => $parseData["olahraga"],
        'keteranganOlahraga' => $parseData["keteranganOlahraga"],
        'kondisi' => $parseData["kondisi"],
        'keteranganKondisi' => $parseData["keteranganKondisi"],
        'rujukan' => $parseData["butuhRujukan"],
        'created_at' => date('Y-m-d'),
    	]);

      $arr = array (
        'status'=> "OK",
        'response'=> 200,
        'message'=> "input data sukses"
      );
      return json_encode($arr);
    }
}
