<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css " />
<style media="screen">
.paginate_button.current{
  background: #c27803 !important;
  color: #fff;
}
.dataTables_filter{
  margin-bottom: 10px;
}
.dataTables_filter > label > input{
  border: 1px solid grey;
  border-radius: 5px;
  padding: 5px;
  padding-left: 10px;
}
</style>
<h2 class="text-2xl font-semibold leading-tight">Daftar Pasien COVID-19</h2>
<div class="mt-10 shadow-md p-5">
  <div class="mb-5">
    <span onclick="toggleTableContent('all')" class="cursor-pointer tracking-wider text-white bg-blue-500 px-4 py-1 text-sm rounded leading-loose mx-2 font-semibold" title="">
      <i class="fas fa-search"></i> {{$jumlahPasien}} ALL
    </span>

    <span onclick="toggleTableContent('dirawat')" class="cursor-pointer tracking-wider text-white bg-red-500 px-4 py-1 text-sm rounded leading-loose mx-2 font-semibold" title="">
      <i class="fas fa-exclamation-triangle"></i> {{$jumlahPasienDirawat}} Dirawat
    </span>

    <span onclick="toggleTableContent('sembuh')" class="cursor-pointer tracking-wider text-white bg-green-500 px-4 py-1 text-sm rounded leading-loose mx-2 font-semibold" title="">
      <i class="fas fa-heart" aria-hidden="true"></i> {{$jumlahPasienSembuh}} Sembuh
    </span>
  </div>

  <table id="table_id" class="display shadow-md">
    <thead>
      <tr>
        <th class="bg-pink-900 text-white">Nama Pasien</th>
        <th class="bg-pink-900 text-white">umur</th>
        <th class="bg-pink-900 text-white">gejala</th>
        <th class="bg-pink-900 text-white">keterangan</th>
        <th class="bg-pink-900 text-white">status pasien</th>
        <th class="bg-pink-900 text-white">action</th>
      </tr>
    </thead>
    <tbody id="table_body">
      <?php
      foreach ($pasien->original["data"] as $key => $value) { ?>
        <tr onclick="openModalSecond(<?php echo $value['id'] ?>)" class="cursor-pointer <?php echo $value['flag'] ? 'dirawat' : 'sembuh'; ?>">
          <td>{{$value['nama']}}</td>
          <td>{{$value['umur']}}</td>
          <td>{{$value['gejala']}}</td>
          <td>{{$value['status']}}</td>
          <td>
            <?php if ($value['flag']) {
              echo "Dalam Perawatan";
            }else {
              echo "Sembuh";
            } ?>
          </td>
          <td>
            <?php if ($value['flag']) {
              echo "Checkup";
            }else {
              echo "Cetak Sertifikat";
            } ?>
          </th>

        </tr>
        <?php
      }
      ?>

    </tbody>
  </table>
</div>

<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript">
var js_variable  = '<?php echo json_encode($pasien->original["data"]);?>';
var pasienSemua = JSON.parse(js_variable);
var pasienSembuhs = [];
var pasienDirawats = [];
function openModalSecond(id){
  window.open('http://localhost:8000/profile/'+id, '_blank');
}
function toggleTableContent(value) {
  var tableDatatables = $('#table_id').DataTable();
  tableDatatables.destroy();
  $('#table_id tbody').empty();
  var v = document.getElementsByClassName("dirawat");
  var tableBody = document.getElementById('table_body');
  if (value === 'dirawat') {
    pasienDirawats.map(o => {
      var tr = document.createElement('tr');
      tr.classList.add("cursor-pointer");
      if (o.flag) {
        tr.classList.add("dirawat");
      }else {
        tr.classList.add("sembuh");
      }
      tr.onclick = () => openModalSecond(o.id);
      tableBody.appendChild(tr);
      let td = document.createElement('td');
      let td1 = document.createElement('td');
      let td2 = document.createElement('td');
      let td3 = document.createElement('td');
      let td4 = document.createElement('td');
      let td5 = document.createElement('td');
      tr.appendChild(td);
      tr.appendChild(td1);
      tr.appendChild(td2);
      tr.appendChild(td3);
      tr.appendChild(td4);
      tr.appendChild(td5);
      var txt = document.createTextNode(o.nama);
      var txt1 = document.createTextNode(o.umur);
      var txt2 = document.createTextNode(o.gejala);
      var txt3 = document.createTextNode(o.status);
      var txt4 = document.createTextNode('');
      var txt5 = document.createTextNode('');
      if (o.flag) {
        txt4 = document.createTextNode('Dalam Perawatan');
        txt5 = document.createTextNode('Checkup');
      }else {
        txt4 = document.createTextNode('Sembuh');
        txt5 = document.createTextNode('Cetak Sertifikat');
      }

      td.appendChild(txt);
      td1.appendChild(txt1);
      td2.appendChild(txt2);
      td3.appendChild(txt3);
      td4.appendChild(txt4);
      td5.appendChild(txt5);
    })
  }else if (value === 'sembuh'){
    pasienSembuhs.map(o => {
      var tr = document.createElement('tr');
      tr.classList.add("cursor-pointer");
      if (o.flag) {
        tr.classList.add("dirawat");
      }else {
        tr.classList.add("sembuh");
      }
      tr.onclick = () => openModalSecond(o.id);
      tableBody.appendChild(tr);
      let td = document.createElement('td');
      let td1 = document.createElement('td');
      let td2 = document.createElement('td');
      let td3 = document.createElement('td');
      let td4 = document.createElement('td');
      let td5 = document.createElement('td');

      tr.appendChild(td);
      tr.appendChild(td1);
      tr.appendChild(td2);
      tr.appendChild(td3);
      tr.appendChild(td4);
      tr.appendChild(td5);

      var txt = document.createTextNode(o.nama);
      var txt1 = document.createTextNode(o.umur);
      var txt2 = document.createTextNode(o.gejala);
      var txt3 = document.createTextNode(o.status);
      var txt4 = document.createTextNode('');
      var txt5 = document.createTextNode('');

      if (o.flag) {
        txt4 = document.createTextNode('Dalam Perawatan');
        txt5 = document.createTextNode('Checkup');
      }else {
        txt4 = document.createTextNode('Sembuh');
        txt5 = document.createTextNode('Cetak Sertifikat');
      }
      td.appendChild(txt);
      td1.appendChild(txt1);
      td2.appendChild(txt2);
      td3.appendChild(txt3);
      td4.appendChild(txt4);
      td5.appendChild(txt5);

    })
  }else {
    pasienSemua.map(o => {
      var tr = document.createElement('tr');
      tr.classList.add("cursor-pointer");
      if (o.flag) {
        tr.classList.add("dirawat");
      }else {
        tr.classList.add("sembuh");
      }
      tr.onclick = () => openModalSecond(o.id);
      tableBody.appendChild(tr);
      let td = document.createElement('td');
      let td1 = document.createElement('td');
      let td2 = document.createElement('td');
      let td3 = document.createElement('td');
      let td4 = document.createElement('td');
      let td5 = document.createElement('td');

      tr.appendChild(td);
      tr.appendChild(td1);
      tr.appendChild(td2);
      tr.appendChild(td3);
      tr.appendChild(td4);
      tr.appendChild(td5);

      var txt = document.createTextNode(o.nama);
      var txt1 = document.createTextNode(o.umur);
      var txt2 = document.createTextNode(o.gejala);
      var txt3 = document.createTextNode(o.status);
      var txt4 = document.createTextNode('');
      var txt5 = document.createTextNode('');

      if (o.flag) {
        txt4 = document.createTextNode('Dalam Perawatan');
        txt5 = document.createTextNode('Checkup');
      }else {
        txt4 = document.createTextNode('Sembuh');
        txt5 = document.createTextNode('Cetak Sertifikat');
      }
      td.appendChild(txt);
      td1.appendChild(txt1);
      td2.appendChild(txt2);
      td3.appendChild(txt3);
      td4.appendChild(txt4);
      td5.appendChild(txt5);

    })
  }
  rebuildTable();
}
function rebuildTable(){
  $('#table_id').DataTable({
    "responsive": true,
    "processing": true,
    "language":{
      "decimal":        "",
      "emptyTable":     "Tidak ada data yang tersedia pada tabel ini",
      "info":           "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
      "infoEmpty":      "Menampilkan 0 sampai 0 dari 0 data",
      "infoFiltered":   "(disaring dari _MAX_ data keseluruhan)",
      "infoPostFix":    "",
      "thousands":      ",",
      "lengthMenu":     "Tampilkan _MENU_ Data",
      "loadingRecords": "Loading...",
      "processing":     "Sedang memproses...",
      "search":         "Cari:",
      "zeroRecords":    "Tidak ada data yang cocok",
      "paginate": {
        "first":      "First",
        "last":       "Last",
        "next":       "Next",
        "previous":   "Prev"
      },
      "aria": {
        "sortAscending":  ": activate to sort column ascending",
        "sortDescending": ": activate to sort column descending"
      }
    }
  });
}
$(document).ready( function () {
  pasienSemua.map(o => {
    if (o.flag === 1) {
      pasienDirawats.push(o)
    }else{
      pasienSembuhs.push(o)
    }
  })
  rebuildTable()
} );
</script>
