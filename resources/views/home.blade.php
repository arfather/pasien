<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="{{ mix('css/app.css') }}" rel="stylesheet">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css " />
  <script src="{{ mix('js/app.js') }}"></script>

  <title>Layanan</title>
  <style media="screen">
  .bg-app {
    background-image: url('https://images.unsplash.com/photo-1589070127509-ea788341cc5b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=889&q=80');
  }
  </style>
</head>
<body>
  <div class="flex flex-row justify-center items-center min-h-screen bg-app bg-center bg-no-repeat bg-cover" style="height: 100vh; width: 100%">
    <div class="p-5 flex flex-row-reverse absolute right-0 top-0">
      <a href="{{ route('logout') }}" class="bg-yellow-500 rounded-full font-bold text-white px-4 py-3 transition duration-300 ease-in-out hover:bg-yellow-600">
        Logout
      </a>
    </div>
    <div class="container mx-auto">
      <div>

        <div>
          <section class="text-gray-600 body-font ">
            <div class="container px-5 mx-auto">
              <div class="flex flex-wrap w-full mb-8">
                <div class="w-full mb-6 lg:mb-0">
                  <h1 class="text-white sm:text-4xl text-5xl font-medium title-font mb-2">Services</h1>
                  <div class="h-1 w-20 bg-yellow-500 rounded"></div>
                </div>
              </div>
              <div class="flex flex-wrap -m-4">
                <div class="p-2 lg:w-1/3 w-1/2 cursor-pointer hover:bg-yellow-600" onclick="openModal(1)">
                  <div class="flex rounded-lg h-full bg-pink-900 p-8 flex-col">
                    <div class="flex items-center mb-3">
                      <div class="w-8 h-8 mr-3 inline-flex items-center justify-center rounded-full bg-yellow-500 text-white flex-shrink-0">
                        <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                          <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"></path>
                          <circle cx="12" cy="7" r="4"></circle>
                        </svg>
                      </div>
                      <h2 class="text-white text-lg title-font font-medium">Pendaftaran Pasien Baru</h2>
                    </div>
                    <div class="flex-grow">
                      <p class="leading-relaxed text-base text-white">Mendaftarkan pasien yang akan dilakukan pemantauan</p>
                    </div>
                  </div>
                </div>

                <div class="p-2 lg:w-1/3 w-1/2 cursor-pointer hover:bg-yellow-600" onclick="openModal(3)">
                  <div class="flex rounded-lg h-full bg-pink-900 p-8 flex-col">
                    <div class="flex items-center mb-3">
                      <div class="w-8 h-8 mr-3 inline-flex items-center justify-center rounded-full bg-yellow-500 text-white flex-shrink-0">
                        <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                          <circle cx="6" cy="6" r="3"></circle>
                          <circle cx="6" cy="18" r="3"></circle>
                          <circle cx="18" cy="10" r="3"></circle>
                        </svg>
                      </div>
                      <h2 class="text-white text-lg title-font font-medium">Daftar Pasien</h2>
                    </div>
                    <div class="flex-grow">
                      <p class="leading-relaxed text-base text-white">Menampilkan daftar pasien yang masih dirawat dan juga pasien yang telah sembuh</p>
                    </div>
                  </div>
                </div>

                <div class="p-2 lg:w-1/3 w-1/2 cursor-pointer hover:bg-yellow-600" onclick="openModal(2)">
                  <div class="flex rounded-lg h-full bg-pink-900 p-8 flex-col">
                    <div class="flex items-center mb-3">
                      <div class="w-8 h-8 mr-3 inline-flex items-center justify-center rounded-full bg-yellow-500 text-white flex-shrink-0">
                        <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                          <path d="M22 12h-4l-3 9L9 3l-3 9H2"></path>
                        </svg>
                      </div>
                      <h2 class="text-white text-lg title-font font-medium">Cetak Sertifikat</h2>
                    </div>
                    <div class="flex-grow">
                      <p class="leading-relaxed text-base text-white">Digunakan untuk mencetak sertifikat apabila pasien telah dinyatakan sembuh </p>
                    </div>
                  </div>
                </div>


              </div>
            </div>
          </section>
          <!-- Modal HTML embedded directly into document -->
            <div id="ex1" class="modal" style="height: 90vh;width: 95vw;max-width: 100vw;">
              @include('modalPasienBaru')
            </div>

            <div id="ex2" class="modal"  style="height: 60vh;width: 80vw;max-width: 100vw;">
              @include('modalSertifikat')
            </div>

            <div id="ex3" class="modal"  style="height: 90vh;width: 95vw;max-width: 100vw;">
              @include('modalDaftarPasien', ['pasien' => $pasien, 'jumlahPasien' => $jumlahPasien,
              'jumlahPasienDirawat' => $jumlahPasienDirawat,
              'jumlahPasienSembuh' => $jumlahPasienSembuh,])
            </div>
            <script type="text/javascript">
            function openModal(params) {
              $("#ex"+params).modal({
               fadeDuration: 200,
              });
            }

              // $("#ex1").modal({
              //   fadeDuration: 100
              // });
              // $("#ex2").modal({
              //   fadeDuration: 100
              // });
              // $("#ex3").modal({
              //   fadeDuration: 100
              // });
            </script>

        </div>
      </div>
    </div>
    <!-- Remember to include jQuery :) -->

    <!-- jQuery Modal -->
  </div>
</div>
</body>
</html>
