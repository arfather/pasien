
<section>
  <div class="w-full text-white" id="checkup" style="display: block">
    <div class="h-2 bg-pink-900"></div>
    <div class="header text-black">
      Cetak Sertifikat
    </div>
    <small class="text-black">  <?php echo date("d F Y"); ?></small>
    <form id="form-cetak">
      <div class="flex items-center justify-center" style="min-height: 50vh;">
        <div class="container mx-24 bg-white rounded shadow-lg bg-grey-900 rounded-lg">
          <div class="px-12 py-6 text-black">
            <div class="divide-y divide-gray-200">
              <div class="py-8 text-base leading-6 space-y-4 text-gray-700 sm:text-lg sm:leading-7">
                <div class="relative">
                  <input autocomplete="off" id="nama" name="nama" type="text" class="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600" placeholder="Nama Personel" max="<?php echo date('Y-m-d') ?>" />
                  <label for="nama" class="absolute left-0 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:text-gray-600 peer-focus:text-sm" style="top: -15px">Nama Pasien</label>
                </div>
                <div class="relative">
                  <input autocomplete="off" id="tanggalLahir" name="tanggalLahir" type="date" class="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600" placeholder="Tanggal Lahir" max="<?php echo date('Y-m-d') ?>" />
                  <label for="tanggalLahir" class="absolute left-0 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:text-gray-600 peer-focus:text-sm" style="top: -15px">Tanggal Lahir</label>
                </div>
                <div class="flex flex-row-reverse mt-20">
                  <button id="cetak" class="bg-yellow-500 text-white rounded-md px-2 py-1">Cetak</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

  </div>
  <script type="text/javascript">
  $('#cetak').click(function(e) {
    let form = [
      "nama",
      "tanggalLahir",
    ]
    e.preventDefault();
    var fieldValuePairs = $('#form-cetak').serializeArray();
    if (fieldValuePairs[0].value !== '' && fieldValuePairs[1].value !== '') {
      console.log(fieldValuePairs);
      window.open('http://localhost:8000/cetak-sertifikat/'+fieldValuePairs[0].value+'/'+fieldValuePairs[1].value, '_blank');
    } else {
      alert("Lengkapi data terlebih dahulu")
    }

  });
  </script>
</section>
