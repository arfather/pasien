<meta name="csrf-token" content="{{ csrf_token() }}">
<h1 class="text-2xl font-semibold">Pendaftaran Pasien Baru</h1>
<div class="flex flex-col justify-center" style="min-height: 87vh;">
  <div class="relative py-3 sm:mx-auto">
    <div
    class="absolute inset-0 bg-gradient-to-r from-pink-600 to-pink-900 shadow-lg transform -skew-y-6 sm:skew-y-0 sm:-rotate-6 sm:rounded-3xl">
  </div>
  <form id="form">
    <div class="grid grid-cols-2 gap-4">
      <div class="relative px-4 py-10 bg-white shadow-lg sm:rounded-3xl sm:p-20">
        <div class=" mx-auto">
          <div>
            <h1 class="text-2xl font-semibold">Biodata Pasien</h1>
          </div>
          <div class="divide-y divide-gray-200">
            <div class="py-8 text-base leading-6 space-y-4 text-gray-700 sm:text-lg sm:leading-7">
              <div class="relative">
                <input autocomplete="off" id="nama" name="nama" type="text" class="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600" placeholder="Email address" />
                <label for="nama" class="absolute left-0 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:text-gray-600 peer-focus:text-sm" style="top: -15px">Nama Pasien</label>
              </div>
              <div class="relative">
                <input autocomplete="off" id="tanggalLahir" name="tanggalLahir" type="date" class="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600" placeholder="Tanggal Lahir" max="<?php echo date('Y-m-d') ?>" />
                <label for="tanggalLahir" class="absolute left-0 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:text-gray-600 peer-focus:text-sm" style="top: -15px">Tanggal Lahir</label>
              </div>
              <div class="relative">
                <input autocomplete="off" id="alamat" name="alamat" type="text" class="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600" placeholder="Alamat" />
                <label for="alamat" class="absolute left-0 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:text-gray-600 peer-focus:text-sm" style="top: -15px">Alamat</label>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div class="relative px-4 py-10 bg-white shadow-lg sm:rounded-3xl sm:p-10">
        <div class=" mx-auto">
          <div>
            <h1 class="text-2xl font-semibold">Keluhan Akibat COVID-19</h1>
          </div>
          <div class="divide-y divide-gray-200">
            <div class="py-8 text-base leading-6 space-y-4 text-gray-700 sm:text-lg sm:leading-7">
              <div class="relative">
                <input autocomplete="off" id="tanggalCheckUp" name="tanggalCheckUp" type="date" class="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none focus:borer-rose-600" placeholder="tanggalCheckUp" max="<?php echo date('Y-m-d') ?>" />
                <label for="tanggalCheckUp" class="absolute left-0 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:text-gray-600 peer-focus:text-sm" style="top: -15px">Tanggal Terakhir Pengecekan Covid</label>
              </div>
              <div class="border-b-2 border-gray-300">
                Gejala
                <div class="flex flex-row">
                  <label class="flex radio p-2 cursor-pointer">
                    <input class="my-auto transform scale-125" type="radio" value="Tidak Ada Gejala" name="gejala" />
                    <div class="title px-2">tidak ada gejala</div>
                  </label>

                  <label class="flex radio p-2 cursor-pointer">
                    <input class="my-auto transform scale-125" type="radio" value="Gejala Ringan" name="gejala" />
                    <div class="title px-2">gejala ringan (batuk ringan)</div>
                  </label>

                  <label class="flex radio p-2 cursor-pointer">
                    <input class="my-auto transform scale-125" type="radio" value="Gejala Berat" name="gejala" />
                    <div class="title px-2">gejala berat (flu, batuk, demam, lemas)</div>
                  </label>
                </div>
              </div>

              <div class="border-b-2 border-gray-300">
                type
                <div class="flex flex-row">
                  <label class="flex radio p-2 cursor-pointer">
                    <input class="my-auto transform scale-125" type="radio" value="antigen" name="type" />
                    <div class="title px-2">Antigen</div>
                  </label>
                  <label class="flex radio p-2 cursor-pointer">
                    <input class="my-auto transform scale-125" type="radio" value="PCR" name="type" />
                    <div class="title px-2">PCR</div>
                  </label>
                </div>
              </div>
              <div class="border-b-2 border-gray-300">
                vendor
                <div class="flex flex-row">
                  <label class="flex radio p-2 cursor-pointer">
                    <input class="my-auto transform scale-125" type="radio" value="RS" name="vendor" />
                    <div class="title px-2">RS</div>
                  </label>
                  <label class="flex radio p-2 cursor-pointer">
                    <input class="my-auto transform scale-125" type="radio" value="Clinic" name="vendor" />
                    <div class="title px-2">Clinic</div>
                  </label>
                  <div class="relative ml-10">
                    <input autocomplete="off" id="namaVendor" name="namaVendor" type="text" class="peer placeholder-transparent h-10 w-full text-gray-900 focus:outline-none focus:borer-rose-600" placeholder="Email address" />
                    <label for="namaVendor" class="absolute left-0 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-440 peer-placeholder-shown:top-2 transition-all peer-focus:text-gray-600 peer-focus:text-sm" style="top: -15px">Nama Vendor</label>
                  </div>

                </div>
              </div>
              <div class="border-b-2 border-gray-300">
                Status
                <div class="flex flex-row">
                  <label class="flex radio p-2 cursor-pointer">
                    <input class="my-auto transform scale-125" type="radio" value="isolasi mandiri" name="status" />
                    <div class="title px-2">Isolasi mandiri</div>
                  </label>
                  <label class="flex radio p-2 cursor-pointer">
                    <input class="my-auto transform scale-125" type="radio" value="rawat inap" name="status" />
                    <div class="title px-2">rawat Inap RS </div>
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="flex flex-row-reverse mt-20">
          <button id="simpan" class="bg-yellow-500 text-white rounded-md px-2 py-1">Simpan</button>
        </div>
      </div>
    </div>

  </form>

</div>
</div>

<script type="text/javascript">
$('#simpan').click(function(e) {
  let form = [
    "nama",
    "tanggalLahir",
    "alamat",
    "tanggalCheckUp",
    "gejala",
    "type",
    "vendor",
    "namaVendor",
    "status"
  ]
  e.preventDefault();
  var fieldValuePairs = $('#form').serializeArray();
  let nama = fieldValuePairs.find(element => element.name === "nama");
  let tanggalLahir = fieldValuePairs.find(element => element.name === "tanggalLahir");
  let alamat = fieldValuePairs.find(element => element.name === "alamat");
  let tanggalCheckUp = fieldValuePairs.find(element => element.name === "tanggalCheckUp");
  let namaVendor = fieldValuePairs.find(element => element.name === "namaVendor");
  console.log(fieldValuePairs);
  if (fieldValuePairs.length === form.length) {
    if (nama.value !== "" && tanggalLahir.value !== "" && alamat.value !== "" && tanggalCheckUp.value !== "" && namaVendor.value !== "") {
      $.ajax({
          headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: '/pendaftaran',
          dataType : 'json',
          type: 'POST',
          data: JSON.stringify(fieldValuePairs),
          contentType: false,
          processData: false,
          success:function(response) {
            alert(response.message);
            window.location.reload(false);
          },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("gagal menyimpan data");
          }
     });
   }else{
     alert("Lengkapi data terlebih dahulu")
   }
  } else {
    alert("Lengkapi data terlebih dahulu")
  }

});

</script>
