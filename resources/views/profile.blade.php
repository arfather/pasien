<meta name="csrf-token" content="{{ csrf_token() }}">

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="{{ mix('css/app.css') }}" rel="stylesheet">

  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css " />
  <script src="{{ mix('js/app.js') }}"></script>
  <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
  <title>Layanan</title>
  <style media="screen">
  .bg-app {
    background-image: url('https://images.unsplash.com/photo-1589070127509-ea788341cc5b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=889&q=80');
  }
  :root {
    --main-color: #4a76a8;
  }

  .bg-main-color {
    background-color: var(--main-color);
  }

  .text-main-color {
    color: var(--main-color);
  }

  .border-main-color {
    border-color: var(--main-color);
  }
  .step {

    padding: 10px;

    display: flex;
    flex-direction: row;
    justify-content: flex-start;

    background-color: cream;
  }

  .v-stepper {
    position: relative;
    /*   visibility: visible; */
  }


  /* regular step */
  .step .circle {
    background-color: white;
    border: 3px solid gray;
    border-radius: 100%;
    width: 20px;    /* +6 for border */
    height: 20px;
    display: inline-block;
  }

  .step .line {
    top: 23px;
    left: 12px;
    /*   height: 120px; */
    height: 100%;

    position: absolute;
    border-left: 3px solid gray;
  }

  .step.completed .circle {
    visibility: visible;
    background-color: rgb(6,150,215);
    border-color: rgb(6,150,215);
  }

  .step.completed .line {
    border-left: 3px solid rgb(6,150,215);
  }

  .step.active .circle {
    visibility: visible;
    border-color: rgb(6,150,215);
  }

  .step.empty .circle {
    visibility: hidden;
  }

  .step.empty .line {
    /*     visibility: hidden; */
    /*   height: 150%; */
    top: 0;
    height: 150%;
  }


  .step:last-child .line {
    border-left: 3px solid white;
    z-index: -1; /* behind the circle to completely hide */
  }

  .contentStepper {
    margin-left: 20px;
    display: inline-block;
  }
  .tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
  }

  .tooltip .tooltiptext {
    visibility: hidden;
    width: 120px;
    background-color: #555;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 10px 0;
    position: absolute;
    z-index: 1;
    bottom: 125%;
    left: 50%;
    margin-left: -60px;
    opacity: 0;
    transition: opacity 0.3s;
  }

  .tooltip .tooltiptext::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
  }

  .tooltip:hover .tooltiptext {
    visibility: visible;
    opacity: 1;
  }
  </style>
</head>
<body>

  <div class="bg-gray-100">
    <div class="w-full text-white bg-pink-900">
      <div x-data="{ open: false }"
      class="flex flex-col max-w-screen-xl mx-auto md:items-center md:justify-between md:flex-row">
      <div class="p-4 flex flex-row items-center justify-between">
        <a href="/home"
        class="text-lg font-semibold tracking-widest uppercase rounded-lg focus:outline-none focus:shadow-outline">Biodata Pasien COVID-19</a>
        <button class="md:hidden rounded-lg focus:outline-none focus:shadow-outline" @click="open = !open">
          <svg fill="currentColor" viewBox="0 0 20 20" class="w-6 h-6">
            <path x-show="!open" fill-rule="evenodd"
            d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM9 15a1 1 0 011-1h6a1 1 0 110 2h-6a1 1 0 01-1-1z"
            clip-rule="evenodd"></path>
            <path x-show="open" fill-rule="evenodd"
            d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
            clip-rule="evenodd"></path>
          </svg>
        </button>
      </div>
    </div>
  </div>
  <!-- End of Navbar -->

  <div class="container mx-auto my-5 p-5">
    <div class="md:flex no-wrap md:-mx-2 ">
      <!-- Right Side -->
      <div class="w-full mx-2 h-64">
        <!-- Profile tab -->
        <!-- About Section -->
        <div class="bg-white p-3 shadow-sm rounded-lg">
          <div class="flex items-center space-x-2 font-semibold text-gray-900 leading-8">
            <span clas="text-green-500">
              <svg class="h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
              stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
              d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
            </svg>
          </span>
          <span class="tracking-wide">Data Pasien</span>
        </div>
        <div class="text-gray-700">
          <div class="grid md:grid-cols-2 text-sm">
            <div class="grid grid-cols-2">
              <div class="px-32 py-2 font-semibold">Nama Pasien</div>
              <div class="px-4 py-2">{{$pasien->nama}}</div>
            </div>
            <div class="grid grid-cols-2">
              <div class="px-32 py-2 font-semibold">Gejala</div>
              <div class="px-4 py-2">{{$pasien->gejala}}</div>
            </div>
            <div class="grid grid-cols-2">
              <div class="px-32 py-2 font-semibold">Tanggal Lahir</div>
              <div class="px-4 py-2">{{$pasien->tanggalLahir}}</div>
            </div>
            <div class="grid grid-cols-2">
              <div class="px-32 py-2 font-semibold">Terakhir Periksa</div>
              <div class="px-4 py-2">{{$pasien->lastCheckUp}}</div>
            </div>
            <div class="grid grid-cols-2">
              <div class="px-32 py-2 font-semibold">Umur</div>
              <div class="px-4 py-2">{{$pasien->umur}}</div>
            </div>
            <div class="grid grid-cols-2">
              <div class="px-32 py-2 font-semibold">Vendor</div>
              <div class="px-4 py-2">{{$pasien->vendor}}</div>
            </div>

            <div class="grid grid-cols-2">
              <div class="px-32 py-2 font-semibold">Status</div>
              <div class="px-4 py-2">{{$pasien->status}}</div>
            </div>

            <div class="grid grid-cols-2">
              <div class="px-32 py-2 font-semibold">Nama Vendor</div>
              <div class="px-4 py-2">{{$pasien->namaVendor}}</div>
            </div>

          </div>
        </div>
      </div>
      <!-- End of about section -->

      <div class="my-4"></div>

      <!-- Experience and education -->
      <div class="bg-white p-3 shadow-lg rounded-lg">
        <?php echo date("d F Y"); ?>

        <div class="grid grid-cols-2">
          <div>
            <div class="grid grid-cols-3 mb-5 mt-5">
              <?php if ($pasien->flag) { ?>
                <div onclick="toggleInputOrRiwayat('input')" class="flex items-center space-x-2 font-semibold text-gray-900 leading-8 m-auto cursor-pointer">
                  <span clas="text-green-500">
                    <svg class="h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M9 12h6m-6 4h6m2 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                  </svg>
                </span>
                <span class="tracking-wide">Input Data Checkup</span>
              </div>
            <?php  } ?>
            <div onclick="toggleInputOrRiwayat('riwayat')" class="flex items-center space-x-2 font-semibold text-gray-900 leading-8 m-auto cursor-pointer">
              <span clas="text-green-500">
                <svg class="h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                d="M9 12h6m-6 4h6m2 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
              </svg>
            </span>
            <span class="tracking-wide">Riwayat Checkup</span>
          </div>
          <div onclick="openModal(<?php echo $pasien->flag ?>)" class="flex items-center space-x-2 font-semibold text-gray-900 leading-8 m-auto cursor-pointer">
            <span clas="text-green-500">
              <svg class="h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
              stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
              d="M9 12h6m-6 4h6m2 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
            </svg>
          </span>
          <span class="tracking-wide">Cetak Sertifikat</span>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
  function toggleInputOrRiwayat(value) {
    if (value === 'riwayat') {
      document.getElementById('inputDataTab').style.display = "none";
      document.getElementById('riwayatTab').style.display = "block";

    }else {
      document.getElementById('inputDataTab').style.display = "block";
      document.getElementById('riwayatTab').style.display = "none";
    }
  }
  </script>

  <?php if ($pasien->flag) { ?>
    <!-- START Section Input Data -->
    <div id="inputDataTab">
      <form id="form">
        <div class="flex m-auto"  style="width: 90%">
          <table>
            <thead>
              <th class="border border-gray-300 p-2" style="border-right: 5px solid rgb(210, 214, 220);min-width: 250px;">Checklist detail harian</th>
              <th class="border border-gray-300 text-center" style="min-width: 50px">YA</th>
              <th class="border border-gray-300 text-center" style="min-width: 70px">TIDAK</th>
              <th class="border border-gray-300 pl-2" style="min-width: 50vw;">keterangan</th>
            </thead>
            <tbody>
              <tr>
                <td class="border border-gray-300 pl-2" style="border-right: 5px solid rgb(210, 214, 220)">Minum obat + Vitamin </td>
                <td class="border border-gray-300 text-center">
                  <div class="tooltip"><input class="my-auto transform scale-125" type="radio" value="1" name="minumObat" />
                    <span class="tooltiptext">Sudah Minum Obat</span>
                  </div>
                </td>
                <td class="border border-gray-300 text-center">
                  <div class="tooltip"><input class="my-auto transform scale-125" type="radio" value="0" name="minumObat" />
                    <span class="tooltiptext">Belum Minum Obat</span>
                  </div>
                </td>
                <td class="border border-gray-300">
                  <div class="relative">
                    <input autocomplete="off" id="keteranganMinumObat" name="keteranganMinumObat" type="text" class="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none pl-2 focus:borer-rose-600" />
                  </div>
                </td>
              </tr>
              <tr>
                <td class="border border-gray-300 pl-2" style="border-right: 5px solid rgb(210, 214, 220)">Olahraga</td>
                <td class="border border-gray-300 text-center">
                  <div class="tooltip"><input class="my-auto transform scale-125" type="radio" value="1" name="olahraga" />
                    <span class="tooltiptext">Sudah Olahraga</span>
                  </div>
                </td>
                <td class="border border-gray-300 text-center">
                  <div class="tooltip"><input class="my-auto transform scale-125" type="radio" value="0" name="olahraga" />
                    <span class="tooltiptext">Tidak Olahraga</span>
                  </div>
                </td>
                <td class="border border-gray-300">
                  <div class="relative">
                    <input autocomplete="off" id="keteranganMinumObat" name="keteranganOlahraga" type="text" class="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none pl-2 focus:borer-rose-600" />
                  </div>
                </td>
              </tr>

              <tr>
                <td class="border border-gray-300 pl-2" style="border-right: 5px solid rgb(210, 214, 220)">Kondisi</td>
                <td class="border border-gray-300 text-center">

                  <div class="tooltip"><input class="my-auto transform scale-125" type="radio" value="1" name="kondisi" />
                    <span class="tooltiptext">Kondisi Pasien Normal</span>
                  </div>

                </td>
                <td class="border border-gray-300 text-center">
                  <div class="tooltip"><input class="my-auto transform scale-125" type="radio" value="0" name="kondisi" />
                    <span class="tooltiptext">Kondisi Pasien Memburuk</span>
                  </div>
                </td>
                <td class="border border-gray-300">
                  <div class="relative">
                    <input autocomplete="off" id="keteranganMinumObat" name="keteranganKondisi" type="text" class="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none pl-2 focus:borer-rose-600" />
                  </div>
                </td>
              </tr>

              <tr>
                <td class="border border-gray-300 pl-2" style="border-right: 5px solid rgb(210, 214, 220)">Butuh rujukan</td>
                <td class="border border-gray-300 text-center">
                  <div class="tooltip"><input class="my-auto transform scale-125" type="radio" value="1" name="butuhRujukan" />
                    <span class="tooltiptext">Pasien Butuh Rujukan</span>
                  </div>
                </td>
                <td class="border border-gray-300 text-center">
                  <div class="tooltip"><input class="my-auto transform scale-125" type="radio" value="0" name="butuhRujukan" />
                    <span class="tooltiptext">Pasien normal</span>
                  </div>
                </td>
                <td class="border border-gray-300">
                  <div class="relative">
                    <input autocomplete="off" id="keteranganMinumObat" disabled name="keteranganButuhRujukan" type="text" class="peer placeholder-transparent h-10 w-full border-b-2 border-gray-300 text-gray-900 focus:outline-none pl-2 focus:borer-rose-600" />
                  </div>
                </td>
              </tr>

            </tbody>
          </table>
        </div>
        <div class="flex flex-row-reverse mt-5 mr-20">
          <button id="simpan" class="bg-yellow-500 text-white rounded-md px-2 py-1">Simpan</button>
        </div>
      </form>
    </div>
    <!-- END Section Input Data -->
  <?php } ?>


  <div id="riwayatTab" class="ml-20" style="display: <?php if ($pasien->flag)  { echo 'none'; }else{ echo 'block';} ?>">

    <div class="container">

      <div class="step active">
        <div class="v-stepper">
          <div class="circle"></div>
          <div class="line"></div>
        </div>

        <div class="contentStepper">
          <div class="dateLabel">
            <?php echo date_format(date_create($pasien->created_at), "d F Y"); ?>
          </div>
          <div class="label">
            Pasien Telah Didaftarkan
          </div>
        </div>
      </div>
      <?php foreach ($perawatan as $key => $value) { ?>
        <div class="step active">
          <div class="v-stepper">
            <div class="circle"></div>
            <div class="line"></div>
          </div>
          <div class="contentStepper">
            <div class="dateLabel">
              <?php echo date_format(date_create($value->tanggalCheckUp), "d F Y"); ?>
            </div>
            <div class="label">
              <?php
              if ($value->minumObat) {
                echo "- Telah Minum Obat";
                if ($value->keteranganMinumObat) {
                  echo ", ".$value->keteranganMinumObat;
                }
              }else{
                echo "- Tidak minum obat";
                if ($value->keteranganMinumObat) {
                  echo ", ".$value->keteranganMinumObat;
                }
              }
              echo "<br />";

              if ($value->olahraga) {
                echo "- Sudah Olahraga";
                if ($value->keteranganOlahraga) {
                  echo ", ".$value->keteranganOlahraga;
                }
              }else{
                echo "- Pasien Tidak Melakukan Olahraga";
                if ($value->keteranganOlahraga) {
                  echo ", ".$value->keteranganOlahraga;
                }
              }
              echo "<br />";

              if ($value->kondisi) {
                echo "- Kondisi Pasien Normal";
                if ($value->keteranganKondisi) {
                  echo ", ".$value->keteranganKondisi;
                }
              }else{
                echo "- Kondisi Pasien Memburuk";
                if ($value->keteranganKondisi) {
                  echo ", ".$value->keteranganKondisi;
                }
              }
              echo "<br />";

              if ($value->kondisi === 0) {
                if ($value->rujukan) {
                  echo "- <b>Pasien Butuh Rujukan</b>";
                }else{
                  echo "- <b>Pasien masih dalam pemantauan </b>";
                }
              }else{
                echo "- tidak butuh rujukan";
              }

              ?>
            </div>
          </div>
        </div>
      <?php } ?>

    </div>
  </div>
</div>
</div>
</div>

<div id="ex" class="modal"  style="height: 10vh;width: 35vw;max-width: 100vw;">
  Dengan mencetak Sertifikat maka pasien sudah dinyatakan sembuh <br> <br>
  <span class="cursor-pointer hover:bg-gray-200" onclick="gotoCetak(<?php echo $pasien->id ?>)">lanjutkan</span>
</div>
<script type="text/javascript">
var js_variable  = '<?php echo json_encode($pasien);?>';
var pasienSemua = JSON.parse(js_variable);
function openModal(params) {
  if (params) {
    $("#ex").modal({
      fadeDuration: 200,
    });
  }else{
    window.open('http://localhost:8000/cetak/'+pasienSemua.id, '_blank');
    window.location.reload(false);
  }
}
function gotoCetak(id) {
  window.open('http://localhost:8000/cetak/'+id, '_blank');
  window.location.reload(false);
}

$('#simpan').click(function(e) {

  let form = [
    "minumObat",
    "keteranganMinumObat",
    "olahraga",
    "keteranganOlahraga",
    "kondisi",
    "keteranganKondisi",
    "butuhRujukan"
  ]
  e.preventDefault();
  var fieldValuePairs = $('#form').serializeArray();
  if (fieldValuePairs.length === form.length) {
    fieldValuePairs.push({name: "idPasien", value: '<?php echo $pasien->id ?>'})
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '/checkup',
      dataType : 'json',
      type: 'POST',
      data: JSON.stringify(fieldValuePairs),
      contentType: false,
      processData: false,
      success:function(response) {
        alert(response.message);
        window.location.reload(false);
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("gagal menyimpan data");
      }
    });
  } else {
    alert("Lengkapi data terlebih dahulu")
  }

});
</script>

</body>
</html>
