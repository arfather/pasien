<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePasienTable extends Migration
{
    public function up()
    {
        Schema::create('pasien',function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('nama');
            $table->date('tanggalLahir');
            $table->text('alamat')->nullable();
            $table->string('umur')->nullable();
            $table->string('gejala')->nullable();
            $table->date('lastCheckUp')->nullable();
            $table->string('type')->nullable();
            $table->string('vendor')->nullable();
            $table->string('namaVendor')->nullable();
            $table->string('status')->nullable();
            $table->boolean('flag')->nullable();
            $table->timestamps();
        });
        Schema::create('perawatan',function(Blueprint $table){
            $table->increments("idPerawatan");
            $table->unsignedBigInteger('idPasien');
            $table->foreign('idPasien')->references('id')->on('pasien');
            $table->date('tanggalCheckUp')->nullable();
            $table->boolean('minumObat')->nullable();
            $table->string('keteranganMinumObat')->nullable();
            $table->boolean('olahraga')->nullable();
            $table->string('keteranganOlahraga')->nullable();
            $table->boolean('kondisi')->nullable();
            $table->string('keteranganKondisi')->nullable();
            $table->boolean('rujukan')->nullable();
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::dropIfExists('perawatan');
        Schema::dropIfExists('pasien');
    }
}
